package com.yoshiky.atapp.repository.impl;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.yoshiky.atapp.bean.PersonaBean;
import com.yoshiky.atapp.bean.RespuestaBean;
import com.yoshiky.atapp.dto.PersonaDto;
import com.yoshiky.atapp.mapper.PersonaMapper;
import com.yoshiky.atapp.repository.PersonaDao;
import com.yoshiky.atapp.util.Constante;

@Repository
public class PersonaDaoImpl implements PersonaDao {

	@Autowired
	DataSource dataSource;

	@Override
	public RespuestaBean create(PersonaDto request) throws SQLException {
		
		RespuestaBean response = new RespuestaBean();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				
		String query = "INSERT  INTO ATDB.PERSONA (nombres,apePaterno,apeMaterno,documento,tipoDocumento,direccion,telefono1)" 
		+ "VALUES (?,?,?,?,?,?,?)";
		
		jdbcTemplate.update(query, 
				request.getNombres(),
				request.getApePaterno(),
				request.getApeMaterno(),
				request.getDocumento(),
				request.getTipoDocumento(),
				request.getDireccion(),
				request.getTelefono1());
				
		response.setCode(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
		response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_EXITO_CREAR_PERSONA);
		
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PersonaDto find(Object object) throws SQLException {
		
		PersonaBean request = (PersonaBean) object;
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		String sql = "SELECT * FROM ATDB.PERSONA WHERE ID = ?";

		PersonaDto persona = (PersonaDto) jdbcTemplate.queryForObject(sql, new Object[] {request.getId()}, new PersonaMapper());

		return persona;
	}

	@Override
	public RespuestaBean update(PersonaDto request) throws SQLException {
		
		RespuestaBean response = new RespuestaBean();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				
		String query = "UPDATE  ATDB.PERSONA "
				+ "SET nombres = ?, "
				+ "apePaterno = ?, "
				+ "apeMaterno = ?, "
				+ "documento = ?, "
				+ "tipoDocumento = ?, "
				+ "direccion = ?, "
				+ "telefono1 = ? "
				+ "WHERE id = "+request.getId()+"";
		
		jdbcTemplate.update(query, 
				request.getNombres(),
				request.getApePaterno(),
				request.getApeMaterno(),
				request.getDocumento(),
				request.getTipoDocumento(),
				request.getDireccion(),
				request.getTelefono1());
				
		response.setCode(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
		response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_EXITO_ACTUALIZAR_PERSONA);
		
		return response;
	}

	@Override
	public RespuestaBean delete(PersonaDto request) throws SQLException {

		RespuestaBean response = new RespuestaBean();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		String query = "DELETE FROM  ATDB.PERSONA WHERE id = ?";

		jdbcTemplate.update(query, request.getId());

		response.setCode(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
		response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_EXITO_ELIMINAR_PERSONA);

		return response;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PersonaDto> findAll() throws SQLException {
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		
		String query = "SELECT * FROM ATDB.PERSONA";

		List<PersonaDto> lstPersonas = jdbcTemplate.query(query, new PersonaMapper());
		return lstPersonas;
	}
	
}
