package com.yoshiky.atapp.repository;

import java.sql.SQLException;
import java.util.List;

import com.yoshiky.atapp.bean.RespuestaBean;

public interface EntityDao<T> {

	RespuestaBean create(T t) throws SQLException;

	RespuestaBean update(T t) throws SQLException;

	RespuestaBean delete(T t) throws SQLException;

	T find(Object t) throws SQLException;

	List<T> findAll() throws SQLException;

}
