package com.yoshiky.atapp.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.yoshiky.atapp.bean.PersonaBean;
import com.yoshiky.atapp.bean.RespuestaBean;
import com.yoshiky.atapp.dto.PersonaDto;
import com.yoshiky.atapp.repository.PersonaDao;
import com.yoshiky.atapp.service.PersonaService;
import com.yoshiky.atapp.util.Constante;

@Service
public class PersonaServiceImpl implements PersonaService {

	private static final Logger logger = LoggerFactory.getLogger(PersonaServiceImpl.class);
	
	@Autowired
	PersonaDao personaDao;
	
	@Override
	public RespuestaBean findPerson(PersonaBean request) {
		logger.info("[Modulo WS] - Inicia capa service: findPerson");
		RespuestaBean response = new RespuestaBean();
		PersonaDto persona = new PersonaDto();
		try {
			logger.info("[Modulo WS] - Inicia capa repository: findPerson");
			persona = personaDao.find(request);

			if (persona != null) {
				response.setCode(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
				response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_EXITO_BUSCAR_PERSONA);
				response.setValue(persona);
			} else {
				response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
				response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_NO_EXITO_BUSCAR_PERSONA);

			}
		} catch (SQLException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		} catch (EmptyResultDataAccessException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		}
		return response;
	}

	@Override
	public RespuestaBean createPerson(PersonaDto request) {
		logger.info("[Modulo WS] - Inicia capa service: createPerson");
		RespuestaBean response = new RespuestaBean();
		try {
			logger.info("[Modulo WS] - Inicia capa repository: createPerson");
			response = personaDao.create(request);

			if (StringUtils.isBlank(response.getCode()) || StringUtils.isBlank(response.getMessage())) {
				response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
				response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_NO_EXITO_CREAR_PERSONA);
			}
		} catch (SQLException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		} catch (EmptyResultDataAccessException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		}
		return response;
	}

	@Override
	public RespuestaBean updatePerson(PersonaDto request) {
		logger.info("[Modulo WS] - Inicia capa service: updatePerson");
		RespuestaBean response = new RespuestaBean();
		try {
			logger.info("[Modulo WS] - Inicia capa repository: updatePerson");
			response = personaDao.update(request);

			if (StringUtils.isBlank(response.getCode()) || StringUtils.isBlank(response.getMessage())) {
				response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
				response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_NO_EXITO_ACTUALIZAR_PERSONA);
			}
		} catch (SQLException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		} catch (EmptyResultDataAccessException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		}
		return response;
	}

	@Override
	public RespuestaBean deletePerson(PersonaDto request) {
		logger.info("[Modulo WS] - Inicia capa service: deletePerson");
		RespuestaBean response = new RespuestaBean();
		try {
			logger.info("[Modulo WS] - Inicia capa repository: deletePerson");
			response = personaDao.delete(request);

			if (StringUtils.isBlank(response.getCode()) || StringUtils.isBlank(response.getMessage())) {
				response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
				response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_NO_EXITO_ELIMINAR_PERSONA);
			}
		} catch (SQLException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		} catch (EmptyResultDataAccessException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		}
		return response;
	}

	@Override
	public RespuestaBean findAllPersons() {
		logger.info("[Modulo WS] - Inicia capa service: findAllPersons");
		RespuestaBean response = new RespuestaBean();
		List<PersonaDto> lstPersons = new ArrayList<>();

		try {
			logger.info("[Modulo WS] - Inicia capa repository: findAllPersons");
			lstPersons = personaDao.findAll();

			if (!lstPersons.isEmpty()) {
				response.setCode(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
				response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_EXITO_LISTA_PERSONAS);
				response.setValue(lstPersons);
			} else {
				response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
				response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_NO_EXITO_LISTA_PERSONAS);

			}
		} catch (SQLException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		} catch (EmptyResultDataAccessException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		}

		return response;
	}

}
