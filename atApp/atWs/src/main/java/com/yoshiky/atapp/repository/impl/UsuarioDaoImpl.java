package com.yoshiky.atapp.repository.impl;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.yoshiky.atapp.bean.RespuestaBean;
import com.yoshiky.atapp.bean.UsuarioBean;
import com.yoshiky.atapp.dto.UsuarioDto;
import com.yoshiky.atapp.mapper.UsuarioMapper;
import com.yoshiky.atapp.repository.UsuarioDao;

@Repository
public class UsuarioDaoImpl implements UsuarioDao {

	@Autowired
	DataSource dataSource;

	@SuppressWarnings("unchecked")
	@Override
	public UsuarioDto find(Object object) throws SQLException {
		
		UsuarioBean request = (UsuarioBean) object;
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		
		StringBuilder query = new StringBuilder();
		query.append("SELECT ")
		.append("u.id,")
		.append("p.nombres,")
		.append("p.apePaterno,")
		.append("p.apeMaterno,")
		.append("u.user,")
		.append("u.password ")
		.append("FROM ATDB.USUARIO u ")
		.append("INNER JOIN ATDB.PERSONA p ON p.id = u.persona ")
		.append("WHERE u.USER = '" + request.getUser() + "' AND u.PASSWORD = '"
				+ request.getPassword() + "'");
				
		UsuarioDto usuario = (UsuarioDto) jdbcTemplate.queryForObject(query.toString(), new Object[] {}, new UsuarioMapper());

		return usuario;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UsuarioDto> findAll() throws SQLException {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		
		StringBuilder query = new StringBuilder();
		query.append("SELECT ")
		.append("u.id,")
		.append("p.nombres,")
		.append("p.apePaterno,")
		.append("p.apeMaterno,")
		.append("u.user,")
		.append("u.password ")
		.append("FROM ATDB.USUARIO u ")
		.append("INNER JOIN ATDB.PERSONA p ON p.id = u.persona");

		List<UsuarioDto> lstUsers = jdbcTemplate.query(query.toString(), new UsuarioMapper());
		return lstUsers;
	}

	@Override
	public RespuestaBean create(UsuarioDto t) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaBean update(UsuarioDto t) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaBean delete(UsuarioDto t) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
