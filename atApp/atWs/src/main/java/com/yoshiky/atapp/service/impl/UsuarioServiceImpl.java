package com.yoshiky.atapp.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.yoshiky.atapp.bean.RespuestaBean;
import com.yoshiky.atapp.bean.UsuarioBean;
import com.yoshiky.atapp.dto.UsuarioDto;
import com.yoshiky.atapp.repository.UsuarioDao;
import com.yoshiky.atapp.service.UsuarioService;
import com.yoshiky.atapp.util.Constante;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	private static final Logger logger = LoggerFactory.getLogger(UsuarioServiceImpl.class);

	@Autowired
	UsuarioDao usuarioDao;

	@Override
	public RespuestaBean findAllUsers() {

		logger.info("[Modulo WS] - Inicia capa service: findAllUsers");
		RespuestaBean response = new RespuestaBean();
		List<UsuarioDto> lstUsers = new ArrayList<>();

		try {
			logger.info("[Modulo WS] - Inicia capa repository: findAllUsers");
			lstUsers = usuarioDao.findAll();

			if (!lstUsers.isEmpty()) {
				response.setCode(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
				response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_EXITO_LISTA_USUARIOS);
				response.setValue(lstUsers);
			} else {
				response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
				response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_NO_EXITO_LISTA_USUARIOS);

			}
		} catch (SQLException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		} catch (EmptyResultDataAccessException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		}

		return response;
	}

	@Override
	public RespuestaBean validateUser(UsuarioBean request) {

		logger.info("[Modulo WS] - Inicia capa service: validateUser");
		RespuestaBean response = new RespuestaBean();
		UsuarioDto usuario = new UsuarioDto();

		try {

			if (request != null) {
				// Se obtiene usuario desde DB
				usuario = usuarioDao.find(request);
				// Se valida el usuario obtenido
				if (usuario != null && StringUtils.isNotBlank(usuario.getUser())
						&& StringUtils.isNotBlank(usuario.getPassword())) {
					response.setCode(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
					response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_EXITO_VALIDACION_LOGIN);
					response.setValue(usuario);
				} else {
					response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
					response.setMessage(Constante.REST_MSG.MSG_RESPUESTA_NO_EXITO_VALIDACION_LOGIN);
				}
			}

			logger.info("[Modulo WS] - Termina capa service: validateUser");

		} catch (SQLException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		} catch (EmptyResultDataAccessException e) {
			logger.error("Error!: " + e);
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setMessage(Constante.REST_ERROR.MSG_ERROR_DB);
		}

		return response;
	}

}
