package com.yoshiky.atapp.service;

import com.yoshiky.atapp.bean.PersonaBean;
import com.yoshiky.atapp.bean.RespuestaBean;
import com.yoshiky.atapp.dto.PersonaDto;

public interface PersonaService {

	RespuestaBean findPerson(PersonaBean request);

	RespuestaBean createPerson(PersonaDto request);

	RespuestaBean updatePerson(PersonaDto request);
	
	RespuestaBean deletePerson(PersonaDto request);
	
	RespuestaBean findAllPersons();

}
