package com.yoshiky.atapp.service;

import com.yoshiky.atapp.bean.RespuestaBean;
import com.yoshiky.atapp.bean.UsuarioBean;

public interface UsuarioService {

	RespuestaBean findAllUsers();
	
	RespuestaBean validateUser(UsuarioBean request);
}
