package com.yoshiky.atapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yoshiky.atapp.bean.RespuestaBean;
import com.yoshiky.atapp.service.UsuarioService;

@RestController
public class UsuarioController {

	private static final Logger logger = LoggerFactory.getLogger(UsuarioController.class);

	@Autowired
	UsuarioService usuarioService;

	@RequestMapping(value = "/findallusers", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<RespuestaBean> findAllUsers() {

		logger.info("[Modulo WS] - Inicia capa controller: findAllUsers");

		RespuestaBean response = new RespuestaBean();
		HttpStatus estado = null;
		response = usuarioService.findAllUsers();

		if (response != null) {
			estado = HttpStatus.OK;
		} else {
			estado = HttpStatus.BAD_REQUEST;
		}

		return new ResponseEntity<RespuestaBean>(response, estado);

	}
}
