package com.yoshiky.atapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yoshiky.atapp.bean.PersonaBean;
import com.yoshiky.atapp.bean.RespuestaBean;
import com.yoshiky.atapp.dto.PersonaDto;
import com.yoshiky.atapp.service.PersonaService;

@RestController
public class PersonaController {

	private static final Logger logger = LoggerFactory.getLogger(PersonaController.class);

	@Autowired
	PersonaService personaService;

	@RequestMapping(value = "/findperson", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<RespuestaBean> findPerson(@RequestBody PersonaBean request) {

		logger.info("[Modulo WS] - Inicia capa controller: findPerson");
		RespuestaBean response = new RespuestaBean();
		HttpStatus estado = null;
		response = personaService.findPerson(request);
		if (response != null) {
			estado = HttpStatus.OK;
		} else {
			estado = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<RespuestaBean>(response, estado);

	}
	
	@RequestMapping(value = "/createperson", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<RespuestaBean> createPerson(@RequestBody PersonaDto request) {
		logger.info("[Modulo WS] - Inicia capa controller: createPerson");
		RespuestaBean response = new RespuestaBean();
		HttpStatus estado = null;
		response = personaService.createPerson(request);
		if (response != null) {
			estado = HttpStatus.OK;
		} else {
			estado = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<RespuestaBean>(response, estado);
	}
	
	@RequestMapping(value = "/updateperson", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<RespuestaBean> updatePerson(@RequestBody PersonaDto request) {
		logger.info("[Modulo WS] - Inicia capa controller: updatePerson");
		RespuestaBean response = new RespuestaBean();
		HttpStatus estado = null;
		response = personaService.updatePerson(request);
		if (response != null) {
			estado = HttpStatus.OK;
		} else {
			estado = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<RespuestaBean>(response, estado);
	}
	
	@RequestMapping(value = "/deleteperson", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<RespuestaBean> deletePerson(@RequestBody PersonaDto request) {
		logger.info("[Modulo WS] - Inicia capa controller: deletePerson");
		RespuestaBean response = new RespuestaBean();
		HttpStatus estado = null;
		response = personaService.deletePerson(request);
		if (response != null) {
			estado = HttpStatus.OK;
		} else {
			estado = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<RespuestaBean>(response, estado);
	}
	
	@RequestMapping(value = "/findallpersons", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<RespuestaBean> findAllPersons() {

		logger.info("[Modulo WS] - Inicia capa controller: findAllPersons");

		RespuestaBean response = new RespuestaBean();
		HttpStatus estado = null;
		response = personaService.findAllPersons();

		if (response != null) {
			estado = HttpStatus.OK;
		} else {
			estado = HttpStatus.BAD_REQUEST;
		}

		return new ResponseEntity<RespuestaBean>(response, estado);

	}

}
