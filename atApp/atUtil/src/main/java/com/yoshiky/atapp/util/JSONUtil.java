package com.yoshiky.atapp.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class JSONUtil {

	private final static Logger logger = LoggerFactory.getLogger(JSONUtil.class);

	public static ResponseEntity<String> callAPIRest(String nameMethod, String inJSON, HttpMethod httpMethod) {

		// Invocando al servicio API REST
		String URI = Constante.PARAMETRO.REST_HOST + nameMethod;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>(inJSON, headers);
		ResponseEntity<String> outJSON = restTemplate.exchange(URI, httpMethod, entity, String.class);
		logger.info("JSON obtenido del WS consumido: " + outJSON.getBody().toString());

		return outJSON;
	}
	
	public static String getURI(String nameMethod){
		return Constante.PARAMETRO.REST_HOST + nameMethod;
	}

}
