package com.yoshiky.atapp.util;

public abstract class Constante {

	public abstract class PARAMETRO {

		public static final String REST_HOST = "http://localhost:8052/atWs/";
		public static final String COD_EJECUCION = "1";
		public static final String COD_RESPUESTA_EXITO = "0";
		public static final String COD_RESPUESTA_NO_EXITO = "1";

	}

	public abstract class REST_URI {

		public static final String LOGINCHECK = "restserver/logincheck";
		public static final String FINDALLUSERS = "restserver/findallusers";
		public static final String FINDPERSON = "restserver/findperson";
		
	}

	public abstract class REST_MSG {

		public static final String MSG_RESPUESTA_EXITO_VALIDACION_LOGIN = "Validaci�n de usuario y password exitosa.";
		public static final String MSG_RESPUESTA_NO_EXITO_VALIDACION_LOGIN = "Validaci�n de usuario y password no exitosa.";
		
		public static final String MSG_RESPUESTA_EXITO_LISTA_USUARIOS = "La lista contiene usuarios.";
		public static final String MSG_RESPUESTA_NO_EXITO_LISTA_USUARIOS = "La lista se encuetra vac�a.";
		
		public static final String MSG_RESPUESTA_EXITO_BUSCAR_PERSONA = "Se encontr� a la persona.";
		public static final String MSG_RESPUESTA_NO_EXITO_BUSCAR_PERSONA = "No se encontr� a la persona.";
		public static final String MSG_RESPUESTA_EXITO_CREAR_PERSONA = "Se cre� a la persona correctamente.";
		public static final String MSG_RESPUESTA_NO_EXITO_CREAR_PERSONA = "No se pudo crear a la persona.";
		public static final String MSG_RESPUESTA_EXITO_ACTUALIZAR_PERSONA = "Se actualiz� a la persona correctamente.";
		public static final String MSG_RESPUESTA_NO_EXITO_ACTUALIZAR_PERSONA = "No se pudo actualizar a la persona.";
		public static final String MSG_RESPUESTA_EXITO_ELIMINAR_PERSONA = "Se elimin� a la persona correctamente.";
		public static final String MSG_RESPUESTA_NO_EXITO_ELIMINAR_PERSONA = "No se pudo eliminar a la persona.";
		public static final String MSG_RESPUESTA_EXITO_LISTA_PERSONAS = "La lista contiene personas.";
		public static final String MSG_RESPUESTA_NO_EXITO_LISTA_PERSONAS = "La lista se encuetra vac�a.";
		
	}
	
	public abstract class REST_ERROR {

		public static final String MSG_ERROR_DB = "Error al realizar la transacci�n en Base de Datos.";

	}
	
	public abstract class ESTADO {

		public static final String ACTIVO = "Activo";
		public static final String INACTIVO = "Inactivo";

	}

}
