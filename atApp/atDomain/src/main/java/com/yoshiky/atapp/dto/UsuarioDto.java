package com.yoshiky.atapp.dto;

import java.io.Serializable;

public class UsuarioDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String user;
	private String password;
	private PersonaDto persona;

	public UsuarioDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public PersonaDto getPersona() {
		return persona;
	}

	public void setPersona(PersonaDto persona) {
		this.persona = persona;
	}

	@Override
	public String toString() {
		return "UserDto [id=" + id + ", user=" + user + ", password=" + password + ", persona=" + persona + "]";
	}

}
