package com.yoshiky.atapp.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.yoshiky.atapp.dto.PersonaDto;
import com.yoshiky.atapp.dto.UsuarioDto;

@SuppressWarnings("rawtypes")
public class UsuarioMapper implements RowMapper {

	public UsuarioDto mapRow(ResultSet rs, int rowNum) throws SQLException {

		UsuarioDto usuario = new UsuarioDto();
		PersonaDto persona = new PersonaDto();
		
		persona.setId(Long.valueOf(rs.getString("ID")));
		persona.setNombres(rs.getString("NOMBRES"));
		persona.setApePaterno(rs.getString("APEPATERNO"));
		persona.setApeMaterno(rs.getString("APEMATERNO"));
		usuario.setPersona(persona);
		usuario.setUser(rs.getString("USER"));
		usuario.setPassword(rs.getString("PASSWORD"));
		return usuario;

	}

}
