package com.yoshiky.atapp.bean;

import java.io.Serializable;

public class UsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String user;
	private String password;
	private PersonaBean persona;

	public UsuarioBean() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public PersonaBean getPersona() {
		return persona;
	}

	public void setPersona(PersonaBean persona) {
		this.persona = persona;
	}

}
