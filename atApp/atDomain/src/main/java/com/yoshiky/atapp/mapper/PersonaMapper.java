package com.yoshiky.atapp.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.yoshiky.atapp.dto.PersonaDto;

@SuppressWarnings("rawtypes")
public class PersonaMapper implements RowMapper {

	public PersonaDto mapRow(ResultSet rs, int rowNum) throws SQLException {

		PersonaDto persona = new PersonaDto();
		
		persona.setId(Long.valueOf(rs.getString("ID")));
		persona.setNombres(rs.getString("NOMBRES"));
		persona.setApePaterno(rs.getString("APEPATERNO"));
		persona.setApeMaterno(rs.getString("APEMATERNO"));
		persona.setDocumento(rs.getString("DOCUMENTO"));
		persona.setTipoDocumento(rs.getString("TIPODOCUMENTO"));
		persona.setDireccion(rs.getString("DIRECCION"));
		persona.setTelefono1(rs.getString("TELEFONO1"));
		return persona;

	}

}
