package com.yoshiky.atapp.bean;

import java.io.Serializable;

public class RespuestaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String code;
	private String message;
	private Object value;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
