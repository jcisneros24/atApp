package com.yoshiky.atapp.dto;

import java.io.Serializable;

public class PersonaDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String nombres;
	private String apePaterno;
	private String apeMaterno;
	private String documento;
	private String tipoDocumento;
	private String direccion;
	private String telefono1;
	
	String nombreCompleto;

	public PersonaDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApePaterno() {
		return apePaterno;
	}

	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}

	public String getApeMaterno() {
		return apeMaterno;
	}

	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono1() {
		return telefono1;
	}

	public void setTelefono1(String telefono1) {
		this.telefono1 = telefono1;
	}
	
	public String getNombreCompleto() {
		return this.nombreCompleto = nombres + " " + apePaterno;
	}

	@Override
	public String toString() {
		return "PersonaDto [id=" + id + ", nombres=" + nombres + ", apePaterno=" + apePaterno + ", apeMaterno="
				+ apeMaterno + ", documento=" + documento + ", tipoDocumento=" + tipoDocumento + ", direccion="
				+ direccion + ", telefono1=" + telefono1 + "]";
	}
}
