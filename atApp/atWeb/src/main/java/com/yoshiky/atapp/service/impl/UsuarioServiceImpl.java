package com.yoshiky.atapp.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.yoshiky.atapp.bean.RespuestaBean;
import com.yoshiky.atapp.bean.UsuarioBean;
import com.yoshiky.atapp.dto.UsuarioDto;
import com.yoshiky.atapp.service.UsuarioService;
import com.yoshiky.atapp.util.Constante;
import com.yoshiky.atapp.util.JSONUtil;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	final static Logger logger = LoggerFactory.getLogger(UsuarioServiceImpl.class);

	@Override
	public UsuarioDto uriLoginCheck(UsuarioBean request) {

		logger.info("[Modulo Web] - Inicia capa service: m�todo logincheck del API REST.");

		String user = request.getUser();
		String password = request.getPassword();
		UsuarioDto usuario = new UsuarioDto();

		if (!StringUtils.isEmpty(user) && !StringUtils.isEmpty(password)) {

			// Creando JSON de entrada
			logger.info("Inicia construcci�n del JSON de entrada.");
			Map<String, String> map = new HashMap<>();
			map.put("user", user);
			map.put("password", password);

			String inJSON = new Gson().toJson(map);
			logger.info("JSON de entrada creado: " + inJSON);

			// Obteniendo respuesta del servicio
			Gson gson = new Gson();
			ResponseEntity<String> outJSON = JSONUtil.callAPIRest(Constante.REST_URI.LOGINCHECK, inJSON,
					HttpMethod.POST);
			RespuestaBean respuestaBean = gson.fromJson(outJSON.getBody().toString(), RespuestaBean.class);

			String codRptaServicio = respuestaBean.getCode();
			logger.info("C�digo de respuesta obtenido del WS consumido: " + codRptaServicio);

			if (codRptaServicio.equals(Constante.PARAMETRO.COD_RESPUESTA_EXITO)) {

				usuario = gson.fromJson(respuestaBean.getValue().toString(), new TypeToken<UsuarioDto>() {
				}.getType());

			}

		}

		logger.info("[Modulo Web] - Termina capa service: m�todo logincheck del API REST.");

		return usuario;
	}

	@Override
	public List<UsuarioDto> uriFindAllUsers() {

		logger.info("[Modulo Web] - Inicia capa service: m�todo findallusers del API REST.");

		// Obteniendo respuesta del servicio
		Gson gson = new Gson();
		ResponseEntity<String> outJSON = JSONUtil.callAPIRest(Constante.REST_URI.FINDALLUSERS, null, HttpMethod.GET);
		RespuestaBean respuestaBean = gson.fromJson(outJSON.getBody().toString(), RespuestaBean.class);
		List<UsuarioDto> lstUsers = gson.fromJson(respuestaBean.getValue().toString(),
				new TypeToken<List<UsuarioDto>>() {
				}.getType());

		logger.info("[Modulo Web] - Termina capa service: m�todo findallusers del API REST.");
		return lstUsers;
	}

}
