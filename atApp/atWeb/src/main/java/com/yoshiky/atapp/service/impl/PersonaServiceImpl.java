package com.yoshiky.atapp.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.internal.StringMap;
import com.yoshiky.atapp.bean.RespuestaBean;
import com.yoshiky.atapp.dto.PersonaDto;
import com.yoshiky.atapp.service.PersonaService;
import com.yoshiky.atapp.util.Constante;
import com.yoshiky.atapp.util.JSONUtil;

@Service
public class PersonaServiceImpl implements PersonaService {

	final static Logger logger = LoggerFactory.getLogger(PersonaServiceImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public PersonaDto uriFindPerson(String idUser) {

		logger.info("[Modulo Web] - Inicia capa service: metodo findperson del API REST.");

		PersonaDto persona = new PersonaDto();

		try {

			//idUser = "3";
			if (StringUtils.isNotBlank(idUser)) {

				// Creando JSON de entrada
				logger.info("Inicia construcci�n del JSON de entrada.");
				Map<String, String> map = new HashMap<>();
				map.put("id", idUser);

				String inJSON = new Gson().toJson(map);
				logger.info("JSON de entrada creado: " + inJSON);

				// Obteniendo respuesta del servicio
				Gson gson = new Gson();
				ResponseEntity<String> outJSON = JSONUtil.callAPIRest(Constante.REST_URI.FINDPERSON, inJSON,
						HttpMethod.POST);
				RespuestaBean respuestaBean = gson.fromJson(outJSON.getBody().toString(), RespuestaBean.class);

				String codRptaServicio = respuestaBean.getCode();
				logger.info("C�digo de respuesta obtenido del WS consumido: " + codRptaServicio);

				if (codRptaServicio.equals(Constante.PARAMETRO.COD_RESPUESTA_EXITO)) {
					
					StringMap<Object> smPersona = (StringMap<Object>) respuestaBean.getValue();
					
					persona.setNombres((String)smPersona.get("nombres"));
					persona.setApePaterno((String)smPersona.get("apePaterno"));
					persona.setApeMaterno((String)smPersona.get("apeMaterno"));
					persona.setDocumento((String)smPersona.get("documento"));
					persona.setTipoDocumento((String)smPersona.get("tipoDocumento"));
					persona.setDireccion((String)smPersona.get("direccion"));
					persona.setTelefono1((String)smPersona.get("telefono1"));

				}

			}

			logger.info("[Modulo Web] - Termina capa service: metodo findperson del API REST.");

		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		}

		return persona;
	}

}
