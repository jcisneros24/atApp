package com.yoshiky.atapp.service;

import com.yoshiky.atapp.dto.PersonaDto;

public interface PersonaService {
	
	PersonaDto uriFindPerson(String idUser);
	
}
