package com.yoshiky.atapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.yoshiky.atapp.service.PersonaService;

@Controller
public class PersonaController {

	private static final Logger logger = LoggerFactory.getLogger(PersonaController.class);

	@Autowired
	PersonaService personaService;

	@RequestMapping(value = "/createperson", method = RequestMethod.GET)
	public ModelAndView createPerson() {

		logger.info("[Modulo Web] - Inicia capa controller: createperson");
		ModelAndView mav = new ModelAndView();
		mav.setViewName("./persona/crear");
		return mav;

	}
	
	@RequestMapping(value = "/showperson", method = RequestMethod.GET)
	public ModelAndView showPerson(@RequestParam(value = "txtIdUser", required = false) String idUser) {

		logger.info("[Modulo Web] - Inicia capa controller: showperson");
		ModelAndView mav = new ModelAndView();
		mav.addObject("persona", personaService.uriFindPerson(idUser));
		mav.setViewName("./persona/mostrar");
		return mav;

	}
	
	@RequestMapping(value = "/updateperson", method = RequestMethod.GET)
	public ModelAndView updatePerson() {

		logger.info("[Modulo Web] - Inicia capa controller: updateperson");
		return new ModelAndView("./persona/actualizar");

	}

}
