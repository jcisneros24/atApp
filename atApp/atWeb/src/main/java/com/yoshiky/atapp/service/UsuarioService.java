package com.yoshiky.atapp.service;

import java.util.List;

import com.yoshiky.atapp.bean.UsuarioBean;
import com.yoshiky.atapp.dto.UsuarioDto;

public interface UsuarioService {
	
	UsuarioDto uriLoginCheck(UsuarioBean request);
	
	List<UsuarioDto> uriFindAllUsers();
	
}
