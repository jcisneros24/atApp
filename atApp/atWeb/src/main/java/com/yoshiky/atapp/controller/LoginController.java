package com.yoshiky.atapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.yoshiky.atapp.bean.UsuarioBean;
import com.yoshiky.atapp.dto.UsuarioDto;
import com.yoshiky.atapp.service.UsuarioService;

@Controller
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	UsuarioService usuarioService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String redirect() {
		return "redirect:login";
	}

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(Model model, @RequestParam(name = "error", required = false) String error) {

		model.addAttribute("error", error);
		return "login";
	}

	@RequestMapping(value = "main", method = RequestMethod.GET)
	public ModelAndView main(Model model) {

		logger.info("[Modulo Web] - Inicia capa controller: Carga de m�dulo main");
		ModelAndView mav = new ModelAndView();
		mav.setViewName("main");

		return mav;
	}

	@RequestMapping(value = "access", method = RequestMethod.POST)
	public ModelAndView access(@RequestParam(value = "txtUser", required = false) String user,
			@RequestParam(value = "txtPassword", required = false) String password) {

		ModelAndView mav = new ModelAndView();
		logger.info("[Modulo Web] - Inicia capa controller: Validando ingreso al m�dulo main");

		// Creacion del objeto UserDto
		UsuarioBean usuario = new UsuarioBean();
		usuario.setUser(user);
		usuario.setPassword(password);

		// Retorno al invocar el servicio
		UsuarioDto usuarioObtenido = new UsuarioDto();
		usuarioObtenido = usuarioService.uriLoginCheck(usuario);

		if (usuarioObtenido != null) {

			mav.setViewName("main");
			mav.addObject("user", usuarioService.uriLoginCheck(usuario));

		} else {

			mav.setViewName("redirect:/login?error");

		}

		return mav;
	}

	@RequestMapping(value = "close", method = RequestMethod.GET)
	public String close() {
		return redirect();
	}

}
