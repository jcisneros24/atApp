package com.yoshiky.atapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.yoshiky.atapp.service.UsuarioService;

@Controller
public class UsuarioController {

	private static final Logger log = LoggerFactory.getLogger(UsuarioController.class);

	@Autowired
	UsuarioService usuarioService;

	@RequestMapping(value = "/usuario", method = RequestMethod.GET)
	public ModelAndView showAllUsers() {

		log.info("[Modulo Web] - Inicia capa controller: usuario");
		ModelAndView mav = new ModelAndView();
		mav.setViewName("usuario");
		mav.addObject("users", usuarioService.uriFindAllUsers());

		return mav;

	}

}
