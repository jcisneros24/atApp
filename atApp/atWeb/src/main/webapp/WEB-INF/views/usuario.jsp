<%@ include file="../views/base/header.jsp"%>
<%@ include file="../views/base/navbar.jsp"%>

<div class="container">

	<div align="left">
		<h1>Usuarios</h1>
	</div>

	<div align="right">
		<button class="btn btn-success" data-toggle="modal"
			data-target="#registrarUsuario">Nuevo</button>
	</div>
	<br>

	<table class="table table-striped">
		<thead>
			<tr>
				<th class="text-center">#</th>
				<th class="text-center">Nombres</th>
				<th class="text-center">Ape. Paterno</th>
				<th class="text-center">Ape. Materno</th>
				<th class="text-center">Usuario</th>
				<th class="text-center">Password</th>
				<th class="text-right">Acciones</th>
			</tr>
		</thead>

		<c:forEach items="${users}" var="user">
			<tbody>
				<tr>
					<th class="text-center">${user.persona.id}</th>
					<td class="text-center">${user.persona.nombres}</td>
					<td class="text-center">${user.persona.apePaterno}</td>
					<td class="text-center">${user.persona.apeMaterno}</td>
					<td class="text-center">${user.user}</td>
					<td class="text-center">${user.password}</td>
					<td class="text-right">
						<button class="btn btn-info" data-toggle="modal"
							data-target="#mostrarUsuario">Mostrar</button>
						<button class="btn btn-primary" data-toggle="modal"
							data-target="#actualizarUsuario">Actualizar</button>
						<button class="btn btn-danger" data-toggle="modal"
							data-target="#eliminarUsuario">Eliminar</button>
					</td>
				</tr>
			</tbody>
		</c:forEach>

	</table>

</div>

<!-- Invocación a los modal - INICIO -->
<!-- =============================== -->
<div id="registrarUsuario" class="modal fade text-center">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<!-- Modal Header -->
				<h4 class="modal-title">Registrar Usuario</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal Body -->
			<iframe id="iframe-modal" src="createperson"></iframe>
		</div>
	</div>
</div>

<div id="actualizarUsuario" class="modal fade text-center">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<!-- Modal Header -->
				<h4 class="modal-title">Actualizar Usuario</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal Body -->
			<iframe id="iframe-modal" src="updateperson"></iframe>
		</div>
	</div>
</div>

<div id="mostrarUsuario" class="modal fade text-center">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<!-- Modal Header -->
				<h4 class="modal-title">Mostrar Usuario</h4>
									<!-- Text input-->
					<div class="form-group">
						<div class="col-md-4">
							<input id="txtIdUser" name="txtIdUser" type="text"
								class="form-control input-md"
								value="<c:out value="${persona.nombres}"/>"/>
						</div>
					</div>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal Body -->
			<iframe id="iframe-modal" src="showperson"></iframe>
		</div>
	</div>
</div>

<div id="eliminarUsuario" class="modal fade text-center">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<!-- Modal Header -->
				<h4 class="modal-title">Est&aacutes Seguro...</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal Body -->
			<div class="modal-body">
				<p>Que quieres eliminar este Usuario?</p>
			</div>
			<!-- Modal footer -->
			<div class="modal-footer">
				<a href="./parametroConvenioEliminar.html" id="btnYes"
					class="btn confirm" target="_blank">Yes</a> <a href="#"
					data-dismiss="modal" aria-hidden="true" class="btn secondary">No</a>
			</div>
		</div>
	</div>
</div>

<!-- Invocación a los modal - FIN -->
<!-- ============================ -->

<%@ include file="../views/base/footer.jsp"%>