<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- Bootstrap core CSS -->
<spring:url value="resources/css/bootstrap.css" var="bootstrapCss" />
<spring:url value="resources/css/base.css" var="coreCss" />

<!-- Bootstrap core JS -->
<spring:url value="resources/js/jquery-3.2.1.min.js" var="jqueryJs" />
<spring:url value="resources/js/popper.min.js" var="popperJs" />
<spring:url value="resources/js/bootstrap.js" var="bootstrapJs" />