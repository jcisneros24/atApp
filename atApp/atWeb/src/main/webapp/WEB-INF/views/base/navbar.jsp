<nav class="navbar navbar-expand-md navbar-dark navbar-custom">
	<div class="container">
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#nav-content" aria-controls="nav-content"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<!-- Brand -->
		<a class="navbar-brand" href="main">SCG</a>

		<!-- Links -->
		<div class="collapse navbar-collapse" id="nav-content"
			role="navigation">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item dropdown active"><a
					class="nav-link dropdown-toggle" data-toggle="dropdown"
					id="Preview" href="#" role="button" aria-haspopup="true"
					aria-expanded="false"> Mantenimiento </a>
					<div class="dropdown-menu" aria-labelledby="Preview">
						<a class="dropdown-item" href="usuario">Usuarios</a> <a
							class="dropdown-item" href="#">Productos</a> <a
							class="dropdown-item" href="#">Medidas</a>
					</div></li>

				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" data-toggle="dropdown"
					id="Preview" href="#" role="button" aria-haspopup="true"
					aria-expanded="false"> Operaciones </a>
					<div class="dropdown-menu" aria-labelledby="Preview">
						 <a class="dropdown-item" href="#">Contado</a> <a
							class="dropdown-item" href="#">Cr&eacute;dito</a> <a
							class="dropdown-item" href="#">Gastos</a> <a
							class="dropdown-item" href="#">Despacho CMC</a>
					</div></li>
			</ul>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active"><a class="nav-link"><span
						class="badge"><c:set scope="session" var="user"
								value="${user}" />${user.persona.nombreCompleto}</span></a></li>
				<li class="nav-item"><a class="nav-link" href="close">Salir</a></li>
			</ul>
		</div>
	</div>
</nav>
<br>