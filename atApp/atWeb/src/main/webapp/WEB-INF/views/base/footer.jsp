<%@ include file="../base/jstl.jsp" %>
<div class="container">
	<hr>
	<footer class="footer">
		<p class="text-muted text-center">&copy; Yoshiky - AT 1.0</p>
	</footer>
</div>
</body>

<script src="${jqueryJs}"></script>
<script src="${popperJs}"></script>
<script src="${bootstrapJs}"></script>
<script src="${atJs}"></script>

</html>