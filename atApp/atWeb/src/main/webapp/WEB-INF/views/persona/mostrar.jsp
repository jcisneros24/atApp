<%@ include file="../base/jstl.jsp"%>
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />

<body>

	<div class="py-5">
		<div class="container">
			<form class="form-horizontal">
				<fieldset>
										
					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="txtNombres"><b>Nombres:</b></label>
						<div class="col-md-4">
							<input id="txtNombres" name="txtNombres" type="text"
								class="form-control input-md" disabled
								value="<c:out value="${persona.nombres}"/>"/>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="txtApePaterno"><b>Apellido
							Paterno:</b></label>
						<div class="col-md-4">
							<input id="txtApePaterno" name="txtApePaterno" type="text"
								class="form-control input-md" disabled
								value="<c:out value="${persona.apePaterno}"/>"/>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="txtApeMaterno"><b>Apellido
							Materno:</b></label>
						<div class="col-md-4">
							<input id="txtApeMaterno" name="txtApeMaterno" type="text"
								class="form-control input-md" disabled
								value="<c:out value="${persona.apeMaterno}"/>"/>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="txtDocumento"><b>Documento:</b></label>
						<div class="col-md-4">
							<input id="txtDocumento" name="txtDocumento" type="text"
								class="form-control input-md" disabled
								value="<c:out value="${persona.documento}"/>"/>
						</div>
					</div>
										
					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-4 control-label" for="cmbTipoDocumento"><b>Tipo
							Documento:</b></label>
						<div class="col-md-4">
							<select id="cmbTipoDocumento" name="cmbTipoDocumento"
								class="form-control" disabled>
								<option><c:out value="${persona.tipoDocumento}"/></option>
							</select>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="txtDireccion"><b>Direcci&oacuten:</b></label>
						<div class="col-md-4">
							<input id="txtDireccion" name="txtDireccion" type="text"
								class="form-control input-md" disabled
								value="<c:out value="${persona.direccion}"/>"/>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="txtTelefono"><b>Tel&eacutefono:</b></label>
						<div class="col-md-4">
							<input id="txtTelefono" name="txtTelefono" type="text"
								class="form-control input-md" disabled
								value="<c:out value="${persona.telefono1}"/>"/>
						</div>
					</div>

				</fieldset>
			</form>

		</div>
	</div>

</body>

<script src="${jqueryJs}"></script>
<script src="${popperJs}"></script>
<script src="${bootstrapJs}"></script>

</html>