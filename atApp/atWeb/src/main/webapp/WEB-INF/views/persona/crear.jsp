<%@ include file="../base/jstl.jsp"%>
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />

<body>

	<div class="py-5">
		<div class="container">
			<form class="form-horizontal">
				<fieldset>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="txtNombres"><b>Nombres:</b></label>
						<div class="col-md-4">
							<input id="txtNombres" name="txtNombres" type="text"
								placeholder="Ingresar nombres" class="form-control input-md">

						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="txtApePaterno"><b>Apellido
							Paterno:</b></label>
						<div class="col-md-4">
							<input id="txtApePaterno" name="txtApePaterno" type="text"
								placeholder="Ingresar apellido paterno"
								class="form-control input-md">

						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="txtApeMaterno"><b>Apellido
							Materno:</b></label>
						<div class="col-md-4">
							<input id="txtApeMaterno" name="txtApeMaterno" type="text"
								placeholder="Ingresar apellido materno"
								class="form-control input-md">

						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="txtDocumento"><b>Documento:</b></label>
						<div class="col-md-4">
							<input id="txtDocumento" name="txtDocumento" type="text"
								placeholder="Ingresar nro. documento"
								class="form-control input-md" required="">

						</div>
					</div>

					<!-- Select Basic -->
					<div class="form-group">
						<label class="col-md-4 control-label" for="cmbTipoDocumento"><b>Tipo
							Documento:</b></label>
						<div class="col-md-4">
							<select id="cmbTipoDocumento" name="cmbTipoDocumento"
								class="form-control">
								<option value="1">DNI</option>
								<option value="2">RUC</option>
							</select>
						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="txtDireccion"><b>Direcci�n:</b></label>
						<div class="col-md-4">
							<input id="txtDireccion" name="txtDireccion" type="text"
								placeholder="Ingresar direcci�n" class="form-control input-md">

						</div>
					</div>

					<!-- Text input-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="txtTelefono"><b>Tel�fono:</b></label>
						<div class="col-md-4">
							<input id="txtTelefono" name="txtTelefono" type="text"
								placeholder="Ingresar nro. tel�fono"
								class="form-control input-md">

						</div>
					</div>

					<!-- Button (Double) -->
					<div class="form-group">
						<label class="col-md-4 control-label" for="btnAceptar"></label>
						<div class="col-md-8">
							<button id="btnAceptar" name="btnAceptar" class="btn btn-success">Aceptar</button>
							<button id="btnCancelar" name="btnCancelar"
								class="btn btn-danger">Cancelar</button>
						</div>
					</div>

				</fieldset>
			</form>

		</div>
	</div>

</body>

<script src="${jqueryJs}"></script>
<script src="${popperJs}"></script>
<script src="${bootstrapJs}"></script>
<script src="${atJs}"></script>

</html>