<%@ include file="../views/base/header.jsp" %>
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="search-box">
				<div class="caption">
					<h3>Sistema Control de Gastos</h3>
					<p>Alimentos Tagra</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row centered-form center-block">
		<div class="container col-md-4 col-md-offset-4 text-center">

			<div class="form-group">
				<form method="POST" action="access">
					<input type="text" name="txtUser" class="form-control"
						placeholder="Usuario" required autofocus /> <br>
					<input type="password" name="txtPassword" class="form-control"
						placeholder="Password" required /> <br>

					<c:if test="${error != null}">
						<div class="alert alert-danger" role="alert">Acceso
							denegado!</div>
					</c:if>
					<button type="submit" value="login"
						class="btn btn-lg btn-primary btn-block">Entrar</button>
				</form>
			</div>
		</div>
	</div>
</div>
<br>
<%@ include file="../views/base/footer.jsp" %>